package io.vyne.demo.films

import com.orbitalhq.TypeNames.io.vyne.demo.films.StreamingServiceName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = StreamingServiceName,
  imported = true
)
typealias StreamingServiceName = String
