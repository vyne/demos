package io.vyne.demo.films

import com.orbitalhq.TypeNames.io.vyne.demo.films.NetflixFilmId
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = NetflixFilmId,
  imported = true
)
typealias NetflixFilmId = String
