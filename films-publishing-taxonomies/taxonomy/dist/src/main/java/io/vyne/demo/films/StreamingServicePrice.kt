package io.vyne.demo.films

import com.orbitalhq.TypeNames.io.vyne.demo.films.StreamingServicePrice
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = StreamingServicePrice,
  imported = true
)
typealias StreamingServicePrice = BigDecimal
