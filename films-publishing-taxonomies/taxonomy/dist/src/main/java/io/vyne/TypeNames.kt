package io.vyne

import kotlin.String

object TypeNames {
  object io {
    object vyne {
      object demo {
        object films {
          const val FilmId: String = "io.vyne.demo.films.FilmId"

          const val StreamingServiceName: String = "io.vyne.demo.films.StreamingServiceName"

          const val StreamingServicePrice: String = "io.vyne.demo.films.StreamingServicePrice"

          const val NetflixFilmId: String = "io.vyne.demo.films.NetflixFilmId"
        }
      }
    }
  }
}
