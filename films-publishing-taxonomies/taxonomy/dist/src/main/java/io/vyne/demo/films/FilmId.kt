package io.vyne.demo.films

import com.orbitalhq.TypeNames.io.vyne.demo.films.FilmId
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = FilmId,
  imported = true
)
typealias FilmId = Int
