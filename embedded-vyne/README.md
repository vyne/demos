# Embedded Vyne Sample

Vyne Query Engine can be embedded into an existing java application and Vyne queries can be executed in-process against the embedded instance.
This sample, illustrates this in a simple Spring Boot application. To embed Vyne we have annoated the Bootsrapper class with embedded vyne related
annotations:

```
@EnableVyne
@Import(HttpAuthConfig::class)
@EnableConfigurationProperties(VyneSpringCacheConfiguration::class, VyneSpringProjectionConfiguration::class, VyneSpringHazelcastConfiguration::class)
class Bootstrapper
```

Note that the embedded vyne instance still requires a vyne infrastructure to fetch the taxonomy data and hence application.yml contains
the necessary settings to fetch the taxonomy in EUREKA distribution mode:

```
vyne:
  schema:
    publicationMethod: EUREKA
  queryService:
    name: query-service
```

After the startup this application exposes a single REST end point where you can post your Vyne Query. The embedded Vyne instance
will execute the query and return the result. You can interact with this end point through the swagger UI:

* http://localhost:18031/swagger-ui/index.html
