package io.vyne.demos.embeddVyne

import com.orbitalhq.query.SearchFailedException
import com.orbitalhq.query.active.ActiveQueryMonitor
import com.orbitalhq.spring.VyneProvider
import kotlinx.coroutines.flow.Flow
import org.springframework.stereotype.Component
import java.util.UUID

@Component
class InProcessVyne(private val vyneFactory: VyneProvider) {
    /**
     * Public entry point to execute a Query.
     */
    suspend fun queryVyne(vyneQlQuery: String): Flow<Any?> {
        // Set the QueryId
        val queryId = UUID.randomUUID().toString()
        return vyneQLQuery(vyneQlQuery, queryId)
    }

    /**
     * Checks whether application has fetched the expected schema from Vyne Query Server.
     */
    fun schemaInitialised(queryType: String): Boolean {
        return vyneFactory.createVyne().schema.hasType(queryType)
    }

    /**
     * Instantiates in-process vyne instance and execute the given query.
     */
    private suspend fun vyneQLQuery(
            query: String,
            queryId: String
    ): Flow<Any?> {
        val vyne = vyneFactory.createVyne()
        val activeQueryMonitor = ActiveQueryMonitor()
        return try {
            // Query.
            val response = vyne.query(
                    query,
                    queryId = queryId,
                    eventBroker = activeQueryMonitor.eventDispatcherForQuery(queryId, emptyList())
            )
            response.rawResults
        } catch (e: lang.taxi.CompilationException) {
            // Occurs when the Schema does not contain the types required for the query
            throw e
        } catch (e: SearchFailedException) {
            // Occurs When Vyne can't find any paths to execute the query (e.g. Cask is down or doesn't have the relevant 'cask' for the query)
            throw e
        } catch (e: NotImplementedError) {
            // happens when Schema is empty
            throw e
        }
    }
}