package io.vyne.demos.pipelines

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
open class PipelinesDemoApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val app = SpringApplication(PipelinesDemoApp::class.java)
            app.run(*args)
        }
    }

    @Autowired
    fun configureJackson(objectMapper: ObjectMapper) {
    }
}

@RestController
class KafkaPublisher(val kafkaTemplate: KafkaTemplate<String, String>, val objectMapper: ObjectMapper) {

    @PostMapping("/kafka/{topic}", consumes = ["*/*"])
    fun publish(@RequestBody content: String, @PathVariable("topic") topic: String) {
        kafkaTemplate.send(topic, content)
    }
}