#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --verbose --max-rows=10 --replace --profile-file=./qantas-data-profile.json --output-format=json --generation-type=random --output-path=qantas-flight-data.json
