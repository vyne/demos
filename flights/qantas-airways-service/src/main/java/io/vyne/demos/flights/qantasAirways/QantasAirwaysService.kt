package io.vyne.demos.flights.qantasAirways

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import demo.flights.*
import demo.flights.ArrivalTime
import demo.flights.DepartureTime
import demo.flights.FlightId
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.schema.publisher.rsocket.RSocketSchemaPublisherTransport
import com.orbitalhq.schema.rsocket.TcpAddress
import com.orbitalhq.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import lang.taxi.generators.java.TaxiGenerator
import lang.taxi.generators.java.spring.SpringMvcExtension
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit


@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class QantasAirwaysFlightsApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("demo.flights")
            TypeAliasRegister.registerPackage("io.vyne.demos.flights.qantasAirways")
            SpringApplication.run(QantasAirwaysFlightsApp::class.java, *args)
        }
    }

//    @Value("\${server.port}")
//    private val serverPort: String? = null
//
//    @Value("\${spring.application.name}")
//    private val appName: String? = null

    @Bean
    open fun schemaPublisher(): SchemaPublisherService =
        SchemaPublisherService(
            "qantas",
            RSocketSchemaPublisherTransport(
                TcpAddress("localhost", 7655)
            )
        )

    @Autowired
    fun registerSchemas(publisher: SchemaPublisherService) {
        publisher.publish(
            TaxiGenerator()
                .forPackage(QantasAirwaysFlightsService::class.java)
                .addExtension(SpringMvcExtension.forBaseUrl("http://localhost:9870"))
                .generate()
        ).subscribe()
    }
}

@RestController
@Service(documentation = "Provides information about Qantas flights")
class QantasAirwaysFlightsService(private val resourceLoader: ResourceLoader)  {

    private lateinit var flights: List<QantasFlight>
    private lateinit var flightsById: Map<String, QantasFlight>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/qantas-flight-data.json")
        val mapper = jacksonObjectMapper()
                .registerModule(JavaTimeModule())
        val rawFlights: List<QantasFlightRaw> = mapper.readValue(ordersDateFile.inputStream)
        flights = rawFlights.map {
            QantasFlight(
                it.flightId,
                FlightDetails(
                    it.from,
                    it.departureTime
                ),
                FlightDetails(
                    it.to,
                    it.arrivalTime
                ),
                SeatDetails(
                    null,
                    it.totalSeats,
                    it.seatsSold
                )
            )
        }
        flightsById = flights.associateBy { it.flightId }
    }

    @Operation(documentation = "Lists all flights")
    @GetMapping("/flights", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlights(): List<QantasFlight> {
        return flights
    }

    @Operation(documentation = "Looks up a flight using the flight number")
    @GetMapping("/flights/{flightId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlight(@PathVariable("flightId") flightId: FlightId): QantasFlight {
        return flightsById[flightId] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

//@DataType("demo.flights.DepartureAirportCode", imported = true)
//typealias From = String
//
//@DataType("demo.flights.ArrivalAirportCode", imported = true)
//typealias To = String

data class QantasFlightRaw (
    val flightId: String,
    val departureTime: Instant,
    val flightTime: Int,
    val arrivalTime: Instant = departureTime.plus(flightTime.toLong(), ChronoUnit.HOURS),
    val from: String,
    val to: String,
    val seatsAvailable: Int? = null,
    val totalSeats: Int,
    val seatsSold: Int
)

@DataType("demo.flights.QantasFlight", imported = true)
data class QantasFlight (
    val flightId: String,
    val departure: FlightDetails,
    val arrival: FlightDetails,
    val seats: SeatDetails
)

data class FlightDetails(
    val airport: String,
    val time: Instant
)

data class SeatDetails(
    val available: Int? = null,
    val total: Int,
    val sold: Int
)

//@DataType
//data class QantasFlight (
//    val flightId: FlightId,
//    val departureTime: DepartureTime,
//    val flightTime: FlightTime,
//    val arrivalTime: ArrivalTime = departureTime.plus(flightTime.toLong(), ChronoUnit.HOURS),
//    val from: From,
//    val to: To,
//    val seatsAvailable: SeatsAvailable,
//    val totalSeats: TotalSeats,
//    val seatsSold: SeatsSold? = null
//) : Flight

class CustomDateSerializer : StdSerializer<LocalDate>(LocalDate::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd-MMM-yy")
    override fun serialize(value: LocalDate?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(value?.format(formatter))
    }
}