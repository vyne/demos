package io.vyne.demos.flights.visas

import demo.flights.ArrivalCountry
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.schema.publisher.rsocket.RSocketSchemaPublisherTransport
import com.orbitalhq.schema.rsocket.TcpAddress
import com.orbitalhq.spring.VyneSchemaPublisher
import lang.taxi.annotations.*
import lang.taxi.generators.java.TaxiGenerator
import lang.taxi.generators.java.spring.SpringMvcExtension
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class VisaServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("demo.flights")
            TypeAliasRegister.registerPackage("io.vyne.demos.flights.visas")
            SpringApplication.run(VisaServiceApp::class.java, *args)
        }
    }

    @Value("\${server.port}")
    private val serverPort: String? = null

    @Value("\${spring.application.name}")
    private val appName: String? = null

    @Bean
    open fun schemaPublisher(): SchemaPublisherService =
        SchemaPublisherService(
            appName!!,
            RSocketSchemaPublisherTransport(
                TcpAddress("localhost", 7655)
            )
        )

    @Autowired
    fun registerSchemas(publisher: SchemaPublisherService) {
        publisher.publish(
            TaxiGenerator()
                .forPackage(VisaService::class.java)
                .addExtension(SpringMvcExtension.forBaseUrl("http://localhost:${serverPort}"))
                .generate()
        ).subscribe()
    }
}

@RestController
@Service(documentation = "Provides visa information for countries worldwide")
class VisaService(private val resourceLoader: ResourceLoader) {
    private lateinit var visas: List<Visa>
    private lateinit var visasByCountry: Map<String, Visa>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/visa-data.json")
        val mapper = jacksonObjectMapper()
            .registerModule(JavaTimeModule())
        visas = mapper.readValue(ordersDateFile.inputStream)
        visasByCountry = visas.associateBy { it.country }
    }

    @Operation(documentation = "Lists all visa information available")
    @GetMapping("/visas")
    fun getVisas(): List<Visa> {
        return visas
    }

    @Operation(documentation = "Looks up the visa information for a specific country")
    @GetMapping("/visa/{country}")
    fun getVisa(@PathVariable("country") country: ArrivalCountry): Visa {
        return visasByCountry[country] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }

}

@DataType("demo.flights.visa.MaxLengthOfStay")
typealias MaxLengthOfStay = Int

@DataType("demo.flights.visa.NumberOfCovidTestsRequired")
typealias NumberOfCovidTestsRequired = Int

@DataType("demo.flights.visa.ApplicationCost")
typealias ApplicationCost = Double

@DataType("demo.flights.visa.Visa")
data class Visa(
    val country: ArrivalCountry,
    val maxLengthOfStay: MaxLengthOfStay,
    val numberOfCovidTestsRequired: NumberOfCovidTestsRequired,
    val applicationCost: ApplicationCost
)