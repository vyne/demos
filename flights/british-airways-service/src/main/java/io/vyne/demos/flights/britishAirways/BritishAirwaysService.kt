package io.vyne.demos.flights.britishAirways

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import demo.flights.*
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.schema.publisher.rsocket.RSocketSchemaPublisherTransport
import com.orbitalhq.schema.rsocket.TcpAddress
import com.orbitalhq.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import lang.taxi.generators.java.TaxiGenerator
import lang.taxi.generators.java.spring.SpringMvcExtension
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit


@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class BritishAirwaysFlightsApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("demo.flights")
            TypeAliasRegister.registerPackage("io.vyne.demos.flights.britishAirways")
            SpringApplication.run(BritishAirwaysFlightsApp::class.java, *args)
        }
    }

    @Value("\${server.port}")
    private val serverPort: String? = null

    @Value("\${spring.application.name}")
    private val appName: String? = null

    @Bean
    open fun schemaPublisher(): SchemaPublisherService =
        SchemaPublisherService(
            appName!!,
            RSocketSchemaPublisherTransport(
                TcpAddress("localhost", 7655)
            )
        )

    @Autowired
    fun registerSchemas(publisher: SchemaPublisherService) {
        publisher.publish(
            TaxiGenerator()
                .forPackage(BritishAirwaysFlightsService::class.java)
                .addExtension(SpringMvcExtension.forBaseUrl("http://localhost:${serverPort}"))
                .generate()
        ).subscribe()
    }
}

@RestController
@Service(documentation = "Provides information about British Airways flights")
class BritishAirwaysFlightsService(private val resourceLoader: ResourceLoader)  {

    private lateinit var flights: List<BaFlight>
    private lateinit var flightsById: Map<String, BaFlight>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/ba-flight-data.json")
        val mapper = jacksonObjectMapper()
                .registerModule(JavaTimeModule())
        flights = mapper.readValue(ordersDateFile.inputStream)
        flightsById = flights.associateBy { it.flightNum }
    }

    @Operation(documentation = "Lists all flights")
    @GetMapping("/flights", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlights(): List<BaFlight> {
        return flights
    }

    @Operation(documentation = "Looks up a flight using the flight number")
    @GetMapping("/flights/{flightId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlight(@PathVariable("flightId") flightId: FlightId): BaFlight {
        return flightsById[flightId] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

@DataType("demo.flights.FlightId", imported = true)
typealias FlightNum = FlightId

@DataType("demo.flights.DepartureAirportCode", imported = true)
typealias LeavesFrom = DepartureAirportCode

@DataType("demo.flights.ArrivalAirportCode", imported = true)
typealias GoesTo = ArrivalAirportCode


data class BaFlight(
    val flightNum: FlightNum,
    @JsonSerialize(using = CustomInstantSerializer::class)
    val departAt: BaDepartureTime,
    val flightTime: FlightTime,
    val arrivalTime: ArrivalTime = departAt.plus(flightTime.toLong(), ChronoUnit.HOURS),
    val leavesFrom: LeavesFrom,
    val goesTo: GoesTo,
    val seatsSold: SeatsSold? = null,
    val totalSeats: TotalSeats,
    val seatsAvailable: SeatsAvailable
) : Flight

class CustomLocalDateSerializer : StdSerializer<LocalDate>(LocalDate::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd-MMM-yy")
    override fun serialize(value: LocalDate?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(value?.format(formatter))
    }
}

class CustomInstantSerializer : StdSerializer<Instant>(Instant::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss Z").withZone(ZoneId.of("GMT"))
    override fun serialize(value: Instant?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(formatter.format(value))
    }
}