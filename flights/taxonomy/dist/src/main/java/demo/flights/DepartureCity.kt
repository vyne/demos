package demo.flights

import demo.flights.TypeNames.demo.flights.DepartureCity
import lang.taxi.annotations.DataType

@DataType(
  value = DepartureCity,
  imported = true
)
typealias DepartureCity = City
