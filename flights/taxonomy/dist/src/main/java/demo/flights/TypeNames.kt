package demo.flights

import kotlin.String

object TypeNames {
  object demo {
    object flights {
      const val Flight: String = "demo.flights.Flight"

      const val FlightId: String = "demo.flights.FlightId"

      const val DepartureTime: String = "demo.flights.DepartureTime"

      const val ArrivalTime: String = "demo.flights.ArrivalTime"

      const val FlightTime: String = "demo.flights.FlightTime"

      const val WeatherDate: String = "demo.flights.WeatherDate"

      const val DepartureAirportCode: String = "demo.flights.DepartureAirportCode"

      const val ArrivalAirportCode: String = "demo.flights.ArrivalAirportCode"

      const val BaDepartureTime: String = "demo.flights.BaDepartureTime"

      const val Country: String = "demo.flights.Country"

      const val City: String = "demo.flights.City"

      const val AirportCode: String = "demo.flights.AirportCode"

      const val DepartureCity: String = "demo.flights.DepartureCity"

      const val DepartureCountry: String = "demo.flights.DepartureCountry"

      const val TotalSeats: String = "demo.flights.TotalSeats"

      const val SeatsSold: String = "demo.flights.SeatsSold"

      const val SeatsAvailable: String = "demo.flights.SeatsAvailable"

      const val SeatClass: String = "demo.flights.SeatClass"

      const val Airport: String = "demo.flights.Airport"

      const val DepartureAirport: String = "demo.flights.DepartureAirport"

      const val ArrivalCity: String = "demo.flights.ArrivalCity"

      const val ArrivalCountry: String = "demo.flights.ArrivalCountry"

      const val ArrivalAirport: String = "demo.flights.ArrivalAirport"

      const val QantasFlight: String = "demo.flights.QantasFlight"

      const val FormattedDepartureTime_527a08: String = "demo.flights.FormattedDepartureTime_527a08"

      object demo {
        object flights {
          const val `QantasFlight$Departure`: String =
              "demo.flights.demo.flights.QantasFlight${'$'}Departure"

          const val `QantasFlight$Arrival`: String =
              "demo.flights.demo.flights.QantasFlight${'$'}Arrival"

          const val `QantasFlight$Seats`: String =
              "demo.flights.demo.flights.QantasFlight${'$'}Seats"
        }
      }
    }
  }
}
