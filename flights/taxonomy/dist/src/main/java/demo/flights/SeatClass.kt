package demo.flights

import demo.flights.TypeNames.demo.flights.SeatClass
import lang.taxi.annotations.DataType

@DataType(
  value = SeatClass,
  imported = true
)
enum class SeatClass {
  First,

  Business,

  PremiumEconomy,

  Economy
}
