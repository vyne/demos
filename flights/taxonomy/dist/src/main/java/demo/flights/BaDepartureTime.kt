package demo.flights

import demo.flights.TypeNames.demo.flights.BaDepartureTime
import lang.taxi.annotations.DataType

@DataType(
  value = BaDepartureTime,
  imported = true
)
typealias BaDepartureTime = FormattedDepartureTime_527a08
