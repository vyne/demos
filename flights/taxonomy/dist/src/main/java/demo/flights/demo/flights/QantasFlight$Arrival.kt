package demo.flights.demo.flights

import demo.flights.ArrivalAirportCode
import demo.flights.ArrivalTime
import demo.flights.TypeNames.demo.flights.demo.flights.`QantasFlight$Arrival`
import lang.taxi.annotations.DataType

@DataType(
  value = `QantasFlight$Arrival`,
  imported = true
)
open class `QantasFlight$Arrival`(
  val airport: ArrivalAirportCode,
  val time: ArrivalTime
)
