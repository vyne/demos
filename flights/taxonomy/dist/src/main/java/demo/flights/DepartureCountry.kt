package demo.flights

import demo.flights.TypeNames.demo.flights.DepartureCountry
import lang.taxi.annotations.DataType

@DataType(
  value = DepartureCountry,
  imported = true
)
typealias DepartureCountry = Country
