package demo.flights

import demo.flights.TypeNames.demo.flights.ArrivalCountry
import lang.taxi.annotations.DataType

@DataType(
  value = ArrivalCountry,
  imported = true
)
typealias ArrivalCountry = Country
