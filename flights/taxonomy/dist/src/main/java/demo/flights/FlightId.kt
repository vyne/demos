package demo.flights

import demo.flights.TypeNames.demo.flights.FlightId
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = FlightId,
  imported = true
)
typealias FlightId = String
