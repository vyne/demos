package demo.flights

import demo.flights.TypeNames.demo.flights.ArrivalTime
import java.time.Instant
import lang.taxi.annotations.DataType

@DataType(
  value = ArrivalTime,
  imported = true
)
typealias ArrivalTime = Instant
