package demo.flights

import demo.flights.TypeNames.demo.flights.DepartureTime
import java.time.Instant
import lang.taxi.annotations.DataType

@DataType(
  value = DepartureTime,
  imported = true
)
typealias DepartureTime = Instant
