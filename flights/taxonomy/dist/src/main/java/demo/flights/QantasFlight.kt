package demo.flights

import demo.flights.TypeNames.demo.flights.QantasFlight
import demo.flights.demo.flights.`QantasFlight$Arrival`
import demo.flights.demo.flights.`QantasFlight$Departure`
import demo.flights.demo.flights.`QantasFlight$Seats`
import lang.taxi.annotations.DataType

@DataType(
  value = QantasFlight,
  imported = true
)
open class QantasFlight(
  val flightId: FlightId,
  val departure: `QantasFlight$Departure`,
  val arrival: `QantasFlight$Arrival`,
  val seats: `QantasFlight$Seats`
) : Flight
