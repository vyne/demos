package demo.flights

import demo.flights.TypeNames.demo.flights.AirportCode
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = AirportCode,
  imported = true
)
typealias AirportCode = String
