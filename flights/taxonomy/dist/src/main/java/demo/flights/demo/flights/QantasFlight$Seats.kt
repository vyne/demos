package demo.flights.demo.flights

import demo.flights.SeatsAvailable
import demo.flights.SeatsSold
import demo.flights.TotalSeats
import demo.flights.TypeNames.demo.flights.demo.flights.`QantasFlight$Seats`
import lang.taxi.annotations.DataType

@DataType(
  value = `QantasFlight$Seats`,
  imported = true
)
open class `QantasFlight$Seats`(
  val available: SeatsAvailable,
  val sold: SeatsSold,
  val total: TotalSeats
)
