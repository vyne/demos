package demo.flights

import demo.flights.TypeNames.demo.flights.DepartureAirport
import lang.taxi.annotations.DataType

@DataType(
  value = DepartureAirport,
  imported = true
)
open class DepartureAirport(
  val city: DepartureCity,
  val country: DepartureCountry,
  val code: DepartureAirportCode
)
