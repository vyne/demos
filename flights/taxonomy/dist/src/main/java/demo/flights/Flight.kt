package demo.flights

import demo.flights.TypeNames.demo.flights.Flight
import lang.taxi.annotations.DataType

@DataType(
  value = Flight,
  imported = true
)
interface Flight
