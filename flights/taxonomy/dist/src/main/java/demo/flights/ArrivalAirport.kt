package demo.flights

import demo.flights.TypeNames.demo.flights.ArrivalAirport
import lang.taxi.annotations.DataType

@DataType(
  value = ArrivalAirport,
  imported = true
)
open class ArrivalAirport(
  val city: ArrivalCity,
  val country: ArrivalCountry,
  val code: ArrivalAirportCode
)
