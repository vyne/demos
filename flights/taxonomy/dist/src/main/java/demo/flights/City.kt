package demo.flights

import demo.flights.TypeNames.demo.flights.City
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = City,
  imported = true
)
typealias City = String
