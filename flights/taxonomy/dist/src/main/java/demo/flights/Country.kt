package demo.flights

import demo.flights.TypeNames.demo.flights.Country
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = Country,
  imported = true
)
typealias Country = String
