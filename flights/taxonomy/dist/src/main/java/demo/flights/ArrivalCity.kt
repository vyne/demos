package demo.flights

import demo.flights.TypeNames.demo.flights.ArrivalCity
import lang.taxi.annotations.DataType

@DataType(
  value = ArrivalCity,
  imported = true
)
typealias ArrivalCity = City
