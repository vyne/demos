package demo.flights

import demo.flights.TypeNames.demo.flights.TotalSeats
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = TotalSeats,
  imported = true
)
typealias TotalSeats = Int
