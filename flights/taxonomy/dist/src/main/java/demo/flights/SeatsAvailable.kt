package demo.flights

import demo.flights.TypeNames.demo.flights.SeatsAvailable
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = SeatsAvailable,
  imported = true
)
typealias SeatsAvailable = Int
