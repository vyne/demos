package demo.flights.demo.flights

import demo.flights.DepartureAirportCode
import demo.flights.DepartureTime
import demo.flights.TypeNames.demo.flights.demo.flights.`QantasFlight$Departure`
import lang.taxi.annotations.DataType

@DataType(
  value = `QantasFlight$Departure`,
  imported = true
)
open class `QantasFlight$Departure`(
  val airport: DepartureAirportCode,
  val time: DepartureTime
)
