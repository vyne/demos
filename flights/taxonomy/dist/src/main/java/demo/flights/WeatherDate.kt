package demo.flights

import demo.flights.TypeNames.demo.flights.WeatherDate
import lang.taxi.annotations.DataType

@DataType(
  value = WeatherDate,
  imported = true
)
typealias WeatherDate = ArrivalTime
