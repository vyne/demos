package demo.flights

import demo.flights.TypeNames.demo.flights.FlightTime
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = FlightTime,
  imported = true
)
typealias FlightTime = Int
