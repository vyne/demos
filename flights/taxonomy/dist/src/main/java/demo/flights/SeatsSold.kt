package demo.flights

import demo.flights.TypeNames.demo.flights.SeatsSold
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = SeatsSold,
  imported = true
)
typealias SeatsSold = Int
