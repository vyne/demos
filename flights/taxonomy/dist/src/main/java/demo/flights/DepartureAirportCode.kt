package demo.flights

import demo.flights.TypeNames.demo.flights.DepartureAirportCode
import lang.taxi.annotations.DataType

@DataType(
  value = DepartureAirportCode,
  imported = true
)
typealias DepartureAirportCode = AirportCode
