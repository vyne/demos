package demo.flights

import demo.flights.TypeNames.demo.flights.ArrivalAirportCode
import lang.taxi.annotations.DataType

@DataType(
  value = ArrivalAirportCode,
  imported = true
)
typealias ArrivalAirportCode = AirportCode
