package demo.flights

import demo.flights.TypeNames.demo.flights.FormattedDepartureTime_527a08
import lang.taxi.annotations.DataType

@DataType(
  value = FormattedDepartureTime_527a08,
  imported = true
)
typealias FormattedDepartureTime_527a08 = DepartureTime
