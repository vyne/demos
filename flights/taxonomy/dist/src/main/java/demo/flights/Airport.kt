package demo.flights

import demo.flights.TypeNames.demo.flights.Airport
import lang.taxi.annotations.DataType

@DataType(
  value = Airport,
  imported = true
)
open class Airport(
  val city: City,
  val country: Country,
  val code: AirportCode
)
