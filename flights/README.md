
# Flights Demo

# Setup

1. Upload the airport_codes.csv file and set the type to 'DepartureAirport'
2. Upload the airport_codes.csv file again, this time setting the type to 'ArrivalAirport'

## Test Data Generation

In order for the test data to link correctly, we need to ensure that some of the generated data is the same between services.

e.g. For the list of airport codes used for flights in the qantas and BA services, these need to be sourced from the list of airport codes which is uploaded to a cask.

### Airport Codes

Two files exist as the DataHelix data generation library errors when two fields use the InMap constraint. i.e. where we need a departure and arrival airport code from the same list, we use two different files to get around this, one using the InSet constraint instead