#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --verbose --max-rows=50 --replace --profile-file=./emirates-data-profile.json --output-format=json --generation-type=random --output-path=emirates-flight-data.json
