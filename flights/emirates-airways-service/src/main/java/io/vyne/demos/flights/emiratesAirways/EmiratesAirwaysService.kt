package io.vyne.demos.flights.emiratesAirways

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import demo.flights.*
import demo.flights.ArrivalTime
import demo.flights.DepartureTime
import demo.flights.FlightId
import com.orbitalhq.spring.VyneSchemaPublisher
import lang.taxi.annotations.DataType
import lang.taxi.annotations.Operation
import lang.taxi.annotations.Service
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit


@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class EmiratesAirwaysFlightsApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("demo.flights")
            TypeAliasRegister.registerPackage("io.vyne.demos.flights.emiratesAirways")
            SpringApplication.run(EmiratesAirwaysFlightsApp::class.java, *args)
        }
    }
}

@RestController
@Service(documentation = "Provides information about Emirates flights")
class EmiratesAirwaysFlightsService(private val resourceLoader: ResourceLoader)  {

    private lateinit var flights: List<EmiratesFlight>
    private lateinit var flightsById: Map<String, EmiratesFlight>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/emirates-flight-data.json")
        val mapper = jacksonObjectMapper()
                .registerModule(JavaTimeModule())
        flights = mapper.readValue(ordersDateFile.inputStream)
        flightsById = flights.associateBy { it.id }
    }

    @Operation(documentation = "Lists all flights")
    @GetMapping("/flights", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlights(): List<EmiratesFlight> {
        return flights
    }

    @Operation(documentation = "Looks up a flight using the flight number")
    @GetMapping("/flights/{flightId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFlight(@PathVariable("flightId") flightId: FlightId): EmiratesFlight {
        return flightsById[flightId] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

@DataType("demo.flights.DepartureAirportCode", imported = true)
typealias Departs = String

@DataType("demo.flights.ArrivalAirportCode", imported = true)
typealias Arrives = String

@DataType
data class EmiratesFlight (
    val id: FlightId,
    val leavesAt: DepartureTime,
    @JsonIgnore
    val duration: FlightTime,
    val arrivalTime: ArrivalTime = leavesAt.plus(duration.toLong(), ChronoUnit.HOURS),
    val departs: Departs,
    val arrives: Arrives,
    val seatsAvailable: SeatsAvailable,
    val totalSeats: TotalSeats,
    val seatsSold: SeatsSold? = null
) : Flight

class CustomDateSerializer : StdSerializer<LocalDate>(LocalDate::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd-MMM-yy")
    override fun serialize(value: LocalDate?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(value?.format(formatter))
    }
}