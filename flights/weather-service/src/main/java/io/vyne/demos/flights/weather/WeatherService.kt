package io.vyne.demos.flights.weather

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import demo.flights.ArrivalCity
import demo.flights.ArrivalTime
import demo.flights.City
import demo.flights.WeatherDate
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.schema.publisher.rsocket.RSocketSchemaPublisherTransport
import com.orbitalhq.schema.rsocket.TcpAddress
import com.orbitalhq.spring.VyneSchemaPublisher
import lang.taxi.annotations.*
import lang.taxi.generators.java.TaxiGenerator
import lang.taxi.generators.java.spring.SpringMvcExtension
import lang.taxi.generators.kotlin.TypeAliasRegister
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.context.annotation.Bean
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import kotlin.random.Random

@SpringBootApplication
@EnableEurekaClient
@VyneSchemaPublisher
open class WeatherServiceApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            TypeAliasRegister.registerPackage("demo.flights")
            TypeAliasRegister.registerPackage("io.vyne.demos.flights.weather")
            SpringApplication.run(WeatherServiceApp::class.java, *args)
        }
    }

    @Value("\${server.port}")
    private val serverPort: String? = null

    @Value("\${spring.application.name}")
    private val appName: String? = null

    @Bean
    open fun schemaPublisher(): SchemaPublisherService =
        SchemaPublisherService(
            appName!!,
            RSocketSchemaPublisherTransport(
                TcpAddress("localhost", 7655)
            )
        )

    @Autowired
    fun registerSchemas(publisher: SchemaPublisherService) {
        publisher.publish(
            TaxiGenerator()
                .forPackage(WeatherService::class.java)
                .addExtension(SpringMvcExtension.forBaseUrl("http://localhost:${serverPort}"))
                .generate()
        ).subscribe()
    }

}

@RestController
@Service(documentation = "Provides weather information for countries worldwide")
class WeatherService(private val resourceLoader: ResourceLoader) {
    private lateinit var weatherReport: List<WeatherReport>
    private lateinit var weatherReportByCity: Map<String, WeatherReport>

    init {
        val ordersDateFile = resourceLoader.getResource("classpath:test-data/weather-data.json")
        val mapper = jacksonObjectMapper()
            .registerModule(JavaTimeModule())
        weatherReport = mapper.readValue(ordersDateFile.inputStream)
    }

    @Operation(documentation = "Lists all weather information available")
    @GetMapping("/allWeather")
    fun getAllWeather(): List<WeatherReport> {
        return weatherReport
    }

    @Operation(documentation = "Looks up the weather information for a specific city")
    @PostMapping("/weather")
    fun getWeather(request: WeatherRequest): WeatherReport {
        return weatherReport.find {
            it.city == request.city && it.date == LocalDate.ofInstant(request.date, ZoneId.of("GMT"))
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

@ParameterType
data class WeatherRequest(
    val city: ArrivalCity,
    val date: ArrivalTime
)

@DataType("demo.flights.weather.Temperature")
typealias Temperature = Double

@DataType("demo.flights.weather.WeatherReport")
data class WeatherReport(
    val city: City,
    val date: LocalDate
) {
    val averageTemperature: Temperature = (Math.round(Random.nextDouble(0.0, 0.4) * 1000) / 10).toDouble()
}