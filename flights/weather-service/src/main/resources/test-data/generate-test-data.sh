#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --verbose --replace --profile-file=./weather-data-profile.json --output-format=json --generation-type=FULL_SEQUENTIAL --combination-strategy=exhaustive --max-rows=5580 --output-path=weather-data.json
