
 * Start with overview of Icap and Bloomberg orders.
 * Talk about the tagging process, same process as you already follow, just written down.

Showing all the orders, in the raw formats, with gaps

```taxiql
find { BloombergOrder[] }
find { IcapOrder[] }
find { Order[] }
```

```taxiql
find { Order[] } as {
    bloombergOrderId: BloombergOrderId
    icapOrderId: IcapOrderId
    
    isin: Isin
    cusip: Cusip
    
    traderId: TraderId
    icapTradeDate: IcapTradeDate
    bloombergTradeDate: BloombergTradeDate
}[]
```


---

Fill in the gaps

(Add @FirstNotEmpty to custip and isin)

```taxiql
find { Order[] } as {
    bloombergOrderId: BloombergOrderId
    @FirstNotEmpty
    isin: Isin
    icapOrderId: IcapOrderId
    @FirstNotEmpty
    cusip: Cusip
    traderId: TraderId
    icapTradeDate: IcapTradeDate
    bloombergTradeDate: BloombergTradeDate
}[]
```


Enrich:
 * Instrument data
 * Trader data
```taxiql
 * 
 
 ```

```taxiql
import demo.orderFeeds.users.LastName

find { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    
    internalId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    tradingDesk: Desk
    lastName: LastName
}[]
```


SKip this one.
```taxiql
find { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
}[]
```

```taxi
import demo.orderFeeds.TradeDate
import demo.orderFeeds.OrderId
find { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    traderId: TraderId
    tradeDate: TradeDate
}[]
```

Deriving missing calculated data

```taxiql
import demo.orderFeeds.users.LastName

find { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    instrumentId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    traderId: BankTraderId
    desk: Desk
    lastName: LastName
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
}[]
```


Pull in completely unrelated, complex data - like ESG scores

```taxiql
import demo.orderFeeds.users.LastName

find { Order[] } as {
    orderId: OrderId
    @FirstNotEmpty
    isin: Isin
    @FirstNotEmpty
    cusip: Cusip
    instrumentId: InstrumentId
    assetClass: AssetClass
    maturityDate: MaturityDate
    traderId: BankTraderId
    desk: Desk
    lastName: LastName
    total: TotalSize
    remainingSize: RemainingSize
    filledSize: FilledSize
    
    @FirstNotEmpty
    environmentalScore: EnvironmentalScore
    @FirstNotEmpty
    socialScore: SocialScore
    @FirstNotEmpty
    governanceScore: GovernanceScore
    overallEsgScore: Decimal by (EnvironmentalScore + SocialScore + GovernanceScore) / 3
    
}[]
```

Chat query example:

```text
Find all orders, return the id of the order, the total size, filled size, the remaining size, the isin, the cusip,  and the full name (first and last) of the trader who submitted them.  

Fill in the missing isin and cusips.

Calculate the ESG score as the average of Environment, Societal and Governance scores
```

findAll { Order[] } as AntiMoneyLaunderingReportOrder[]



Pipeline:

```json
{
  "name": "standardized-order-feed",
  "id": "standardized-order-feed",
  "description": "Collate all order feeds and normalize to a standard model",

  "input": {
    "type": "query",
    "direction": "INPUT",
    "query": "find { Order[] } as OrderFeed[]",
    "pollSchedule": "0/15 * * * * *",
    "preventConcurrentExecution": true
  },
  "outputs": [
    {
    "type": "jdbc",
    "direction": "OUTPUT",
    "connection": "pipelines",
    "targetTypeName": "OrderFeed",
    "windowDurationMs": 500,
    "writeDisposition": "APPEND"
  }
  ]
}



```

//
    esgScores: {
        @FirstNotEmpty
        env: EnvironmentalScore
        @FirstNotEmpty
        soc: SocialScore
        @FirstNotEmpty
        gov: GovernanceScore
        overallEsgScore: Decimal by (EnvironmentalScore + SocialScore + GovernanceScore) / 3
    }