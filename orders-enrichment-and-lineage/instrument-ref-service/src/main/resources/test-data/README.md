## Tools

Datahelix - Test data generator - https://github.com/finos/datahelix/

## Process

This profile generates the data for both the Instrument service and Instrument ID Mapping service. This is required to ensure that each mapping row associates with one and only one instrument row.

Once generated, a CSV editing tool can be used to strip out the columns not required in each service.

i.e. AssetClass and MaturityDate are removed for the mapping data
Isin and Cusip are removed for the instrument service data