package io.vyne.demos.ordersEnrichmentAndLineage.icapOrderFeed

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.core.io.ResourceLoader
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.math.BigDecimal
import java.time.Instant
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@SpringBootApplication
open class IcapOrderFeedApp {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(IcapOrderFeedApp::class.java, *args)
        }
    }
}

@RestController
class IcapOrderFeedService(private val resourceLoader: ResourceLoader) {
    private lateinit var orders: List<IcapOrder>
    private lateinit var ordersById: Map<String, IcapOrder>

    init {
        val ordersDataFile = resourceLoader.getResource("classpath:test-data/icap-orders-with-date.json")
        val mapper = jacksonObjectMapper()
                .registerModule(JavaTimeModule())
        orders = mapper.readValue(ordersDataFile.inputStream)
        ordersById = orders.associateBy { it.orderId }
    }

    @GetMapping("/orders", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrders(): List<IcapOrder> {
        return orders
    }

    @GetMapping("/orders/{orderId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getOrder(@PathVariable("orderId") orderId: String): IcapOrder {
        return ordersById[orderId] ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)
    }
}

enum class OrderStatus {
    New,
    PartiallyFilled,
    Filled,
    Cancelled,
    Expired
}

enum class OrderType {
    Market, Limit, StopLimit, Iceberg
}

data class IcapOrder(
        val orderId: String,
        val isin: String,
        val cusip: String? = null,
        @JsonSerialize(using = CustomDateSerializer::class)
        val tradeDate: LocalDate,
        val timestamp: Instant,
        val status: OrderStatus,
        val orderType: OrderType,
        val currency: String,
        val filledSize: BigDecimal,
        val remainingSize: BigDecimal,
        val traderId: String
)

class CustomDateSerializer : StdSerializer<LocalDate>(LocalDate::class.java) {
    private val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
    override fun serialize(value: LocalDate?, gen: JsonGenerator, arg2: SerializerProvider?) {
        gen.writeString(value?.format(formatter))
    }
}