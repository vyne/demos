#!/bin/bash

## requires datahelix to be installed
## https://github.com/finos/datahelix/blob/master/docs/GettingStarted.md

datahelix --verbose --max-rows=20 --replace --profile-file=./bloomberg-data-profile.json --output-format=csv --output-path=bloomberg-orders-with-date.csv --generation-type=random
