## Demo

Open services in presentation mode:
Alt+8

 * Show Publishing the schema of the StreamingServicesApp (FilmStreamingServicesApp)
 * Modify the types to semantic types (StreamingMoviesProvider)
   * StreamingProvider data class
   * getStreamingProvidersForFilm method
 * Show the changelog
 * Querying in code (OrbitalQueryDemo)
 * Breaking change 1
   * RottenTomatoesFilmId (ReviewService)
 * Breaking change 2
   * Change get to post (StreamingMoviesProvider) 
 * Streaming data




====

```
find { Film[] } as {
id : FilmId
title : Title

    // Where can I watch this?
    provider: StreamingProviderName
    price: StreamingProviderPrice

    // is it any good?
    review: ReviewText
    score: FilmReviewScore
}[]
```

### Streaming q1
```
stream { NewFilmReleaseAnnouncement }
```

### Streaming q2

```
stream { NewFilmReleaseAnnouncement } as {
id : FilmId
title : Title

    // Where can I watch this?
    provider: StreamingProviderName
    price: StreamingProviderPrice

    // is it any good?
    review: ReviewText
    score: FilmReviewScore

}[]
```