import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.orbital.client.converter.run
import com.orbital.client.find
import com.orbital.client.given
import com.orbital.client.stream
import com.orbital.client.transport.okhttp.http
import com.orbital.client.transport.okhttp.httpStream
import film.Film
import film.types.Title
import films.FilmId
import films.StreamingProviderName
import films.StreamingProviderPrice
import films.reviews.FilmReviewScore
import films.reviews.ReviewText
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import java.time.Duration


data class FilmData(
    val id: FilmId,
    val title: Title,

    val whereCanIWatchThis: StreamingProviderName,
    val cost: StreamingProviderPrice,

    val review:FilmReviewScore,
    val reviewText: ReviewText
)

fun main() {
    find<List<Film>>()
        .asA<List<FilmData>>()
        .run(httpStream("http://localhost:9022"))
        .toFlux()

        .doOnEach { signal ->
            // Log out the response
            println(mapper.writeValueAsString(signal.get()))
        }

        .blockLast(Duration.ofMinutes(10))
}