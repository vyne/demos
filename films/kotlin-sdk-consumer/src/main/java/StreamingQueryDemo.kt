import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.orbital.client.converter.run
import com.orbital.client.stream
import com.orbital.client.transport.okhttp.httpStream
import film.types.Title
import films.FilmId
import films.StreamingProviderName
import films.StreamingProviderPrice
import films.reviews.FilmReviewScore
import films.reviews.ReviewText
import reactor.kotlin.core.publisher.toFlux
import java.time.Duration

val mapper = jacksonObjectMapper().writerWithDefaultPrettyPrinter()


//val whereCanIWatchThis: StreamingProviderName,
//val cost: StreamingProviderPrice,

//val reviewScore: FilmReviewScore,
//val reviewText: ReviewText














data class MyAnnouncementEvent(
    val id: FilmId,
    val title: Title,

    val whereCanIWatchThis: StreamingProviderName,
    val cost: StreamingProviderPrice,

    val review:FilmReviewScore,
    val reviewText: ReviewText
)

fun main() {
    stream<NewFilmReleaseAnnouncement>()
        .asStreamOf<MyAnnouncementEvent>()
        .run(httpStream("http://localhost:9022"))
        .toFlux()

        .doOnEach { signal ->
            // Log out the response
            println(mapper.writeValueAsString(signal.get()))
        }

        .blockLast(Duration.ofMinutes(10))
}