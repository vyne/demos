package io.vyne.demos.films

import com.orbitalhq.PackageMetadata
import com.orbitalhq.schema.publisher.SchemaPublisherService
import com.orbitalhq.schema.publisher.rsocket.RSocketSchemaPublisherTransport
import com.orbitalhq.schema.rsocket.TcpAddress
import lang.taxi.generators.java.spring.SpringTaxiGenerator
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.info.GitProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.stereotype.Component
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
open class FilmStreamingServicesApp {

    companion object {
        val logger = LoggerFactory.getLogger(this::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(FilmStreamingServicesApp::class.java, *args)
        }
    }

    @Autowired
    fun logInfo(@Autowired(required = false) buildInfo: GitProperties? = null) {
        val commit = buildInfo?.shortCommitId ?: "Unknown"
        logger.info("Films API built on commit $commit")
    }
}

@Configuration
open class SwaggerConfig {
    @Bean
    open fun api(): Docket {
        return Docket(DocumentationType.OAS_30)
            .select()
            .apis(RequestHandlerSelectors.basePackage("io.vyne.demos.films"))
            .build()
    }

}

@Component
class RegisterSchemaOnStartup(
    @Value("\${server.port}")
    private val serverPort: String,
    @Value("\${spring.application.name}")
    private val appName: String
) {
    init {
        val publisher = SchemaPublisherService(
            appName,
            RSocketSchemaPublisherTransport(
                TcpAddress("localhost", 7655)
            )
        )
        publisher.publish(
            PackageMetadata.from("io.petflix.demos", appName),
            SpringTaxiGenerator.forBaseUrl("http://localhost:${serverPort}")
                .forPackage(StreamingMoviesProvider::class.java)
                .generate()
        ).subscribe()
    }
}