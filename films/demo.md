# Films Demo

 * SQL Database containing films
 * REST API: "Where can I watch these films?"
 * REST API: Reviews

```
find { Film[] }
```

Restructure to a more nested format...
```
find { Film[] } as {
    film: {
        name: Title
        id : FilmId
        description: Description
    }
    /*
    // Where can I watch this?
    provider: {
        platformName: StreamingProviderName
        price: StreamingProviderPrice
    }
    */
    
    /*
    // Grab some reviews
    review: {
        rating: FilmReviewScore
        review: ReviewText
    }
    */
}[]
```


## Breaking changes

 * Break streaming providers (`StreamingMoviesProvider`)
 * Change ID scheme - `ReviewsService`

## Streaming data

```
stream {  NewFilmReleaseAnnouncement } as {
    news: {
        announcement: NewFilmReleaseAnnouncement
    }

    film: {
        name: Title
        id : FilmId
        description: Description
    }
    provider: {
        platformName: StreamingProviderName
        price: StreamingProviderPrice
    }
    review: {
        rating: FilmReviewScore
        review: ReviewText
    }
}[]
```

via curl:

```bash
curl -X POST 'http://localhost:9022/api/taxiql' \
  -H 'Accept: text/event-stream' \
  -H 'Content-Type: application/taxiql' \
  --data-raw 'stream {  NewFilmReleaseAnnouncement } as {
    news: {
        announcement: NewFilmReleaseAnnouncement
    }

    film: {
        name: Title
        id : FilmId
        description: Description
    }
    provider: {
        platformName: StreamingProviderName
        price: StreamingProviderPrice
    }
    review: {
        rating: FilmReviewScore
        review: ReviewText
    }
}[]'


```