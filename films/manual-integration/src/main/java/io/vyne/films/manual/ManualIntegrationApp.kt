package io.vyne.films.manual

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.repository.CrudRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import javax.persistence.Entity
import javax.persistence.Id
import kotlin.jvm.optionals.getOrNull

@SpringBootApplication
class ManualIntegrationApp {

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(ManualIntegrationApp::class.java, *args);
        }
    }
}

@Entity
data class Film(
    @Id
    val film_id: Int,
    val title: String
)

interface FilmRepository : CrudRepository<Film, Int>

@OptIn(ExperimentalStdlibApi::class)
@RestController
class FilmsService(val repo: FilmRepository, val webclient: WebClient.Builder) {


    @GetMapping("/films/{filmId}")
    fun loadFilmData(@PathVariable("filmId") filmId: Int): Mono<Any> {
        return Mono.just(filmId)
            .subscribeOn(Schedulers.boundedElastic())
            .map { filmId ->
                repo.findById(filmId)
                    .getOrNull()!!
            }
            .flatMap { film ->
                webclient.build()
                    .get()
                    .uri("http://localhost:9981/films/${filmId}/streamingProviders")
                    .retrieve()
                    .bodyToMono<Map<String, Any>>()
                    .map { streamingProvider ->
                        mapOf("film" to film, "streamingProvider" to streamingProvider)
                    }
            }
    }
}