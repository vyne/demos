package films.reviews

import demo.vyne.TypeNames.films.reviews.FilmReviewScore
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = FilmReviewScore,
  imported = true
)
typealias FilmReviewScore = BigDecimal
