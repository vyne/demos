package films.reviews

import demo.vyne.TypeNames.films.reviews.ReviewText
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = ReviewText,
  imported = true
)
typealias ReviewText = String
