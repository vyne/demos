package films.reviews

import demo.vyne.TypeNames.films.reviews.SquashedTomatoesFilmId
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = SquashedTomatoesFilmId,
  imported = true
)
typealias SquashedTomatoesFilmId = String
