package films

import demo.vyne.TypeNames.films.StreamingProviderPrice
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = StreamingProviderPrice,
  imported = true
)
typealias StreamingProviderPrice = BigDecimal
