package films

import demo.vyne.TypeNames.films.StreamingProviderName
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = StreamingProviderName,
  imported = true
)
typealias StreamingProviderName = String
