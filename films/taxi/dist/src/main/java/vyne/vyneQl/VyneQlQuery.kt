package vyne.vyneQl

import demo.vyne.TypeNames.vyne.vyneQl.VyneQlQuery
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = VyneQlQuery,
  imported = true
)
typealias VyneQlQuery = String
