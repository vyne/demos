package film

import demo.vyne.TypeNames.film.Film
import film.types.Description
import film.types.Fulltext
import film.types.LastUpdate
import film.types.Length
import film.types.Rating
import film.types.ReleaseYear
import film.types.RentalDuration
import film.types.RentalRate
import film.types.ReplacementCost
import film.types.SpecialFeatures
import film.types.Title
import films.FilmId
import kotlin.collections.List
import lang.taxi.annotations.DataType
import language.types.LanguageId

@DataType(
  value = Film,
  imported = true
)
open class Film(
  val film_id: FilmId,
  val title: Title,
  val description: Description?,
  val release_year: ReleaseYear?,
  val language_id: LanguageId,
  val original_language_id: LanguageId?,
  val rental_duration: RentalDuration,
  val rental_rate: RentalRate,
  val length: Length?,
  val replacement_cost: ReplacementCost,
  val rating: Rating?,
  val last_update: LastUpdate,
  val special_features: List<SpecialFeatures>?,
  val fulltext: Fulltext
)
