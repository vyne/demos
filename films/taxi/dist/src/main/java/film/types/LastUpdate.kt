package film.types

import demo.vyne.TypeNames.film.types.LastUpdate
import java.time.Instant
import lang.taxi.annotations.DataType

@DataType(
  value = LastUpdate,
  imported = true
)
typealias LastUpdate = Instant
