package film.types

import demo.vyne.TypeNames.film.types.Rating
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = Rating,
  imported = true
)
typealias Rating = String
