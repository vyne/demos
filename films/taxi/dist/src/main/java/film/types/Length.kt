package film.types

import demo.vyne.TypeNames.film.types.Length
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = Length,
  imported = true
)
typealias Length = Int
