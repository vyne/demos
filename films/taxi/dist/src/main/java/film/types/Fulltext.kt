package film.types

import demo.vyne.TypeNames.film.types.Fulltext
import java.lang.Object
import lang.taxi.annotations.DataType

@DataType(
  value = Fulltext,
  imported = true
)
typealias Fulltext = Object
