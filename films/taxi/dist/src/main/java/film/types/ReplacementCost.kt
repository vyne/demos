package film.types

import demo.vyne.TypeNames.film.types.ReplacementCost
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = ReplacementCost,
  imported = true
)
typealias ReplacementCost = BigDecimal
