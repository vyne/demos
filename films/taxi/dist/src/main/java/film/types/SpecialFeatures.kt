package film.types

import demo.vyne.TypeNames.film.types.SpecialFeatures
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = SpecialFeatures,
  imported = true
)
typealias SpecialFeatures = String
