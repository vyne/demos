package film.types

import demo.vyne.TypeNames.film.types.ReleaseYear
import java.lang.Object
import lang.taxi.annotations.DataType

@DataType(
  value = ReleaseYear,
  imported = true
)
typealias ReleaseYear = Object
