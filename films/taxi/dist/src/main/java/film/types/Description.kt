package film.types

import demo.vyne.TypeNames.film.types.Description
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = Description,
  imported = true
)
typealias Description = String
