package film.types

import demo.vyne.TypeNames.film.types.RentalDuration
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = RentalDuration,
  imported = true
)
typealias RentalDuration = Int
