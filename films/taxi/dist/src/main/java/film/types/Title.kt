package film.types

import com.orbital.client.CriteriaSource
import demo.vyne.TypeNames.film.types.Title
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = Title,
  imported = true
)
typealias Title = String



