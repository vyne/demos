package film.types.criteria

import com.fasterxml.jackson.annotation.JsonCreator
import com.orbital.client.CriteriaSource
import demo.vyne.TypeNames
import lang.taxi.annotations.DataType

@JvmInline
@DataType(
    value = TypeNames.film.types.Title,
    imported = true
)
value class Title(val value: String) {
    companion object : CriteriaSource<String> {
        @JsonCreator
        fun create(value: String) = Title(value)
    }

}
