package film.types

import demo.vyne.TypeNames.film.types.RentalRate
import java.math.BigDecimal
import lang.taxi.annotations.DataType

@DataType(
  value = RentalRate,
  imported = true
)
typealias RentalRate = BigDecimal
