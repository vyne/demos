package demo.netflix

import demo.vyne.TypeNames.demo.netflix.NetflixFilmId
import kotlin.String
import lang.taxi.annotations.DataType

@DataType(
  value = NetflixFilmId,
  imported = true
)
typealias NetflixFilmId = Int
