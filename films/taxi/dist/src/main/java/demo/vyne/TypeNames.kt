package demo.vyne

import kotlin.String

object TypeNames {
  object language {
    object types {
      const val LanguageId: String = "language.types.LanguageId"
    }
  }

  object vyne {
    object vyneQl {
      const val VyneQlQuery: String = "vyne.vyneQl.VyneQlQuery"
    }
  }

  object film {
    const val Film: String = "film.Film"

    object types {
      const val ReplacementCost: String = "film.types.ReplacementCost"

      const val LastUpdate: String = "film.types.LastUpdate"

      const val Title: String = "film.types.Title"

      const val Fulltext: String = "film.types.Fulltext"

      const val Description: String = "film.types.Description"

      const val ReleaseYear: String = "film.types.ReleaseYear"

      const val SpecialFeatures: String = "film.types.SpecialFeatures"

      const val Length: String = "film.types.Length"

      const val RentalRate: String = "film.types.RentalRate"

      const val Rating: String = "film.types.Rating"

      const val RentalDuration: String = "film.types.RentalDuration"
    }
  }

  object demo {
    object netflix {
      const val NetflixFilmId: String = "demo.netflix.NetflixFilmId"
    }
  }

  object films {
    const val StreamingProviderName: String = "films.StreamingProviderName"

    const val StreamingProviderPrice: String = "films.StreamingProviderPrice"

    const val FilmId: String = "films.FilmId"

    object reviews {
      const val SquashedTomatoesFilmId: String = "films.reviews.SquashedTomatoesFilmId"

      const val FilmReviewScore: String = "films.reviews.FilmReviewScore"

      const val ReviewText: String = "films.reviews.ReviewText"
    }
  }
}
