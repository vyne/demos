package language.types

import demo.vyne.TypeNames.language.types.LanguageId
import kotlin.Int
import lang.taxi.annotations.DataType

@DataType(
  value = LanguageId,
  imported = true
)
typealias LanguageId = Int
