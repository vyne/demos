

## STreaming query that writes to db

```taxi
import films.reviews.ReviewText
import films.FilmId
import film.types.Title
import NewFilmReleaseAnnouncement

query DataStream {
   stream { NewFilmReleaseAnnouncement } 
   call MyDatabaseService::storeAnnouncement
}

@com.orbitalhq.jdbc.Table(table = "releases" , schema = "public" , connection = "films")
parameter model MyThing {
     title : Title
       id : FilmId
   
       review: ReviewText
}


@com.orbitalhq.jdbc.DatabaseService(connection = "films")
service MyDatabaseService {
    @UpsertOperation
    write operation storeAnnouncement(MyThing):MyThing
}

```